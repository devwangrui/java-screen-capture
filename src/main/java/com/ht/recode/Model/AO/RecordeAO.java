package com.ht.recode.Model.AO;

import lombok.Data;

/**
 * @author 王瑞
 * @description: TODO
 * @date 2021/1/14  10:12
 */
@Data
public class RecordeAO {

    private String cameraName;

    private Long recordeTime;
}


