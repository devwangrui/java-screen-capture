package com.ht.recode.Controller;


import com.ht.recode.Model.AO.RecordeAO;
import com.ht.recode.fxbase.RecorderUtil;
import com.ht.recode.fxbase.VideoRecorder;
import com.ht.recode.utils.CutPicUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author 王瑞
 * @description: TODO
 * @date 2021/1/14  9:19
 */
@Slf4j
@RequestMapping("/recorder/")
@RestController
public class RecorderController {

    @Value("${video.recorder.recorderTime}")
    private Long recorderTime;

    @Value("${video.recorder.img-path}")
    private String imgPath;

    @Value("${video.recorder.file-name}")
    private String videoPath;

    @Autowired
    private RecorderUtil recorderUtil;


    /* @Descreption:功能描述:
     * 〈〉
     * @Param RecordeAO：对象参数里两个属性->1，CameraName 生成的mp4文件文件名称，前面加了一段时间戳 。2，recordeTime：录制时长，如果不填 默认十秒
     * @return: 0代表失败 1代表成功
     * @Author: 王瑞
     * @Date: 2021/1/14 10:33
     */
    @PostMapping("recorder")
    public int recorder(@RequestBody RecordeAO recordeAO){
        int code = 0;
        long l = System.currentTimeMillis();
        //视屏名称 时间戳+名字
        String name = l+"-"+recordeAO.getCameraName();
        try {
            VideoRecorder videoRecorder = recorderUtil.getVideoRecorderInstance(name);
            log.info("开始录屏---------------"+new Date());
            //开始============================================================================================
            videoRecorder.start();

            Long recordeTime = recordeAO.getRecordeTime();
            //定义需要录制的时间长度
            Long reTime = this.recorderTime;
            if (recordeTime!=null){
                //如果传过来的时间不是空，并且大于1秒，就用传过来的时间，不然就用默认录制时长来做时间
                reTime = recordeTime;
                if (recordeTime<1000){
                    //如果传过来的时间不是空，并且小于1秒，就只录1秒
                    reTime = 1000L;
                }
            }

            log.info("这个录屏要录："+reTime/1000+"秒");
            //线程休眠多少秒   录屏就录多少秒
            Thread.sleep(reTime);

            //截图    需要图片的用这个方法取图
            CutPicUtil.cutPic(name,imgPath,"png",videoRecorder.getRectangle());
            log.info("截图保存在这个文件夹下====》："+imgPath);

            //结束============================================================================================
            videoRecorder.stop();
            log.info("录屏结束------------------------------"+new Date());
            log.info("录屏保存在这个文件夹下："+videoPath);

        }catch (Exception e){
            code=1;
        }


        return code;
    }
}
