package com.ht.recode;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class VideoRecordeApplication {

    public static void main(String[] args) {
       // SpringApplication.run(DemoApplication.class, args);
        SpringApplicationBuilder builder = new SpringApplicationBuilder(VideoRecordeApplication.class);
        builder.headless(false).run(args);
    }

}
