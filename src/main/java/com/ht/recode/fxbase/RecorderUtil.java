package com.ht.recode.fxbase;

import com.ht.recode.fxbase.VideoRecorder;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.io.File;

/**
 * @author 王瑞
 * @description: TODO
 * @date 2021/1/13  16:52
 */
@Component
@Data
@ConfigurationProperties(prefix="video.recorder")
public class RecorderUtil {

    /*这4个参数用来定位要录制的屏幕区域  由一个点 和一个矩形来定位
    * startx起点x坐标, starty 起点y坐标,width 录屏的宽度,height录屏的高度;
    * fileName 是视屏录制之后存储的位置
    * */
    private int startx,starty,width,height;
    private String fileName;


   public VideoRecorder getVideoRecorderInstance(String videoName){
       //这4个参数用来定位要录制的屏幕区域  由一个点 和一个Rectangle矩形来定位
       Rectangle rectangle = new Rectangle(startx,starty,width, height); // 截屏的大小
       //如果文件夹没有 就创建一个
       File file = new File(fileName);
       if (!file.isDirectory()){
           file.mkdir();
       }
        //文件夹+文件名
       String  fileHoldName = fileName + videoName;
       //调用录屏API的对象  这个对象就可以开启或者关闭录屏
       VideoRecorder videoRecord  = new VideoRecorder(fileHoldName, true,rectangle);
       return videoRecord;
   }

}
