package com.ht.recode.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * @author 王瑞
 * @description: TODO
 * @date 2021/1/14  10:53
 */
public class CutPicUtil {

    /**
     * 屏幕截图
     * @param imageName 存储图片名称
     * @param path 图片路径
     * @param imgType 图片类型
     * @throws
     * @throws
     */
    public static void cutPic(String imageName,String path,String imgType,Rectangle screenRectangle) throws AWTException, IOException {
       /* Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Rectangle screenRectangle = new Rectangle(screenSize);*/
        Robot robot = new Robot();
        File file = new File(path);
        if (!file.isDirectory()){
            file.mkdir();
        }
        BufferedImage image = robot.createScreenCapture(screenRectangle);
        ImageIO.write(image,imgType, new File(path+imageName+"."+imgType));
    }
}
