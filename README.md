#由于工作中的需求，需要用java来实现当前电脑的录屏功能以及屏幕的截图截屏功能
* 由于习惯了springboot 所以就写在springboot上了，并暴露了一个接口来触发录屏功能

* 核心代码在fxbase包下的VideoRecorder类中 但是代码写的有点乱，使用者可以不必过度关注这个类 而是看看RecorderUtil，我将这个类注入到了ioc容器，这个类的getVideoRecorderInstance可以获取录屏对象 调用这个对象的start()和stop()方法来完成录制和结束，具体过程很简单，可以看RecorderController的调用过程

* 调用RecorderController的recorder接口就可以触发屏幕录制功能，在windows电脑上运行没问题，录制结束之后会抛出一个异常：org.bytedeco.javacv.FrameRecorder$Exception: No video output stream (Is imageWidth > 0 && imageHeight > 0 and has start() been called?)，水平有限，不明所以，但是不影响功能，可以不必理会.

* 录屏：fxbase包下RecorderUtil类中的getVideoRecorderInstance可以获取录屏对象 调用这个对象的start()和stop()方法来完成录制和结束

* 截屏(截图)： utils包下的 CutPicUtil里有个静态方法cutPic  可以用来截屏

* application.properties配置文件可以用来配置去要录制和截屏的区域 以及录屏的默认时长 录像mp4文件和图片png文件的存储位置

* 注：声音也可以录制进去，但是我的需求里不需要 就把那段注掉了 需要的可以打开（VideoRecorder类的caputre()方法）

* 录屏需要的核心依赖-->org.bytedeco javacv和org.bytedeco.javacpp-presets具体请看pom.xml有就行，不必关注怎么用

* 也可以不用springboot这个框架来写，核心类就VideoRecorder 可以拿出来放到普通java类中放在main函数中触发录屏，但需要导核心依赖包

* ####注意：主函数的启动类不是SpringApplication.run(DemoApplication.class, args);不然触发录屏会报错。而是SpringApplicationBuilder builder = new SpringApplicationBuilder(VideoRecordeApplication.class);
                                                                               builder.headless(false).run(args);  
